# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Externalisation de la lib [james-heinrich/getid3](https://packagist.org/packages/james-heinrich/getid3)
- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev

### Fixed

- #4959 Prise en compte du nombre de pagination choisie dans la popin de médiathèque.
- #4957 Accessibilité et UX des boutons de gestion des documents
- #4952 HTML mal formé sur modeles/document_desc.html
- HTML5: Retrait des `CDATA` et `text/javascript` dans les balises `<script>`
